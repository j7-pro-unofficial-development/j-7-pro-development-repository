# PostmarketOS development - Exynos7870

### Currently supported devices:
- <a href="https://wiki.postmarketos.org/wiki/Samsung_Galaxy_J7_Prime_(samsung-on7xelte)">Samsung Galaxy J7 Prime (samsung-on7xelte)
- <a href="https://wiki.postmarketos.org/wiki/Samsung_Galaxy_A2_Core_(samsung-a2corelte)">Samsung Galaxy A2 Core (samsung-a2corelte)

### Preparation

The repository comes with `auto.sh`, a utility script to speed up the development process.

After the repository is cloned, initialize the development packages by running:

```sh
$ ./auto.sh init <device-name>
```
   
A few symlinks of the required packages will appear.

In order to switch to another device, run:

```sh
$ ./auto.sh deinit
$ ./auto.sh init <device-name>
```

### Usage

##### Installation
   
To build and install the packages, simply run:
   
```sh
$ ./auto.sh build
$ ./auto.sh install
```

This command will build the packages and prepare a recovery installation.

In the device, reboot to download mode. Connect the device to the computer via a USB cable. Then run the following command:

```sh
$ ./auto.sh flash
```

The kernel gets flashed to the device, and the device will reboot itself. Have it rebooted to download mode by pressing the appropriate key combination to continue with the flashing process. After the rootfs gets flashed to the device, the device will reboot itself.

##### Kernel Upgrades

To build the kernel, simply run:
   
```sh
$ ./auto.sh build linux
```

In the device, reboot to download mode. Connect the device to the computer via a USB cable. Then run the following command:

```sh
$ ./auto.sh flash kernel
```

The kernel gets flashed to the device. Reboot to system when it's done.

#!/bin/sh

set -e
trap "exit 1" INT



## Configuration - OWN_*

OWN_DEVICE_CHIP="exynos7870"
OWN_DEVICE_ARCH="aarch64"
OWN_DEVICE_FILE=".device-lock"
OWN_APORTS_PATH=".pmaports/"

OWN_DEVICE_NAME="`cat $OWN_DEVICE_FILE 2> /dev/null || true`"



## Configuration - PMB_*

PMB_CONFIG_FILE="`ls ~/.config/pmbootstrap.cfg`"
PMB_DEVICE_TYPE="testing"

PMB_CHROOT_PATH="`cat $PMB_CONFIG_FILE | grep "work = " | sed "s/work = //"`/chroot_native/"
PMB_APORTS_PATH="`cat $PMB_CONFIG_FILE | grep "aports = " | sed "s/aports = //"`/device/$PMB_DEVICE_TYPE/"
PMB_DEVICE_NAME="`cat $PMB_CONFIG_FILE | grep "device = " | sed "s/device = //"`"



# Command - Initialize the device
# Usage: auto.sh init <device-name>

if [ "$1" = "init" ]
then
	if [ -z "$2" ]
	then
		echo -e "\033[1m[!] Usage: auto.sh init <device-name>\033[0m"
		exit 1
	fi

	if [ "$2" = "$OWN_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] The device is already intitalized.\033[0m"
		exit 1
	fi

	if [ ! "$2" = "$OWN_DEVICE_NAME" ] && [ ! -z "$OWN_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Another device is intitalized.\033[0m"
		exit 1
	fi

	CommonPackages="`ls $OWN_APORTS_PATH | grep $OWN_DEVICE_CHIP || true`"
	DevicePackages="`ls $OWN_APORTS_PATH | grep $2 || true`"

	if [ "$DevicePackages" = "" ]
	then
		echo -e "\033[1m[!] The device does not have any packages.\033[0m"
		exit 1
	fi

	echo -e "\033[1m[1/4] Checking device packages of pmaports\033[0m"
	cd $PMB_APORTS_PATH
	for Package in $CommonPackages $DevicePackages
	do
		if [ ! -z "`git status $PMB_APORTS_PATH/$Package --porcelain`" ]
		then
			echo -e "\033[1m[!] Package $Package is unclean.\033[0m"
			exit 1
		fi
	done
	cd $OLDPWD

	echo -e "\033[1m[2/4] Purging existing device packages from pmaports\033[0m"
	for Package in $CommonPackages $DevicePackages
	do
		rm -r $PMB_APORTS_PATH/$Package 2&> /dev/null || true
	done

	echo -e "\033[1m[3/4] Copying device to the pmaports directory\033[0m"
	for Package in $CommonPackages $DevicePackages
	do
		cp -r $OWN_APORTS_PATH/$Package $PMB_APORTS_PATH/
	done

	echo -e "\033[1m[4/4] Creating symlinks to device package directories\033[0m"
	for Package in $CommonPackages $DevicePackages
	do
		ln -fs $PMB_APORTS_PATH/$Package .
	done

	echo "$2" > $OWN_DEVICE_FILE

	exit 0
fi



# Command - Dump the package contents back to the local repository
# Usage: auto.sh dump

if [ "$1" = "dump" ]
then
	if [ -z "$OWN_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Initialize with 'init' first.\033[0m"
		exit 1
	fi

	if [ ! "$OWN_DEVICE_NAME" = "$PMB_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Select the device using 'pmbootstrap init'.\033[0m"
		exit 1
	fi

	Packages="`ls $OWN_APORTS_PATH | grep "$OWN_DEVICE_CHIP\|$OWN_DEVICE_NAME" || true`"

	echo -e "\033[1m[1/3] Generating checksums for device packages\033[0m"
	for Package in $Packages
	do
		pmbootstrap checksum $Package
	done

	echo -e "\033[1m[2/3] Purging existing device packages\033[0m"
	for Package in $Packages
	do
		rm -r $OWN_APORTS_PATH/$Package
	done

	echo -e "\033[1m[3/3] Copying device packages to the repository\033[0m"
	for Package in $Packages
	do
		cp -r $PMB_APORTS_PATH/$Package $OWN_APORTS_PATH/
	done

	exit 0
fi



# Command - Deinitialize the device
# Usage: auto.sh deinit

if [ "$1" = "deinit" ]
then
	if [ -z "$OWN_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Initialize with 'init' first.\033[0m"
		exit 1
	fi

	if [ ! "$OWN_DEVICE_NAME" = "$PMB_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Select the device using 'pmbootstrap init'.\033[0m"
		exit 1
	fi

	Packages="`ls $OWN_APORTS_PATH | grep "$OWN_DEVICE_CHIP\|$OWN_DEVICE_NAME" || true`"

	echo -e "\033[1m[1/5] Generating checksums for device packages\033[0m"
	for Package in $Packages
	do
		pmbootstrap checksum $Package
	done

	echo -e "\033[1m[2/5] Purging existing device packages\033[0m"
	for Package in $Packages
	do
		rm -r $OWN_APORTS_PATH/$Package
	done

	echo -e "\033[1m[3/5] Copying device packages to the repository\033[0m"
	for Package in $Packages
	do
		cp -r $PMB_APORTS_PATH/$Package $OWN_APORTS_PATH/
	done

	echo -e "\033[1m[4/5] Deleting symlinks to device package directories\033[0m"
	for Package in $Packages
	do
		rm $Package
	done

	echo -e "\033[1m[5/5] Purging existing device packages from pmaports\033[0m"
	cd $PMB_APORTS_PATH
	for Package in $Packages
	do
		rm -r $Package
		git restore $Package 2> /dev/null || true
	done
	cd $OLDPWD

	rm $OWN_DEVICE_FILE

	exit 0
fi



# Command - Build the device packages
# Usage: auto.sh build [package]

if [ "$1" = "build" ]
then
	if [ -z "$OWN_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Initialize with 'init' first.\033[0m"
		exit 1
	fi

	if [ ! "$OWN_DEVICE_NAME" = "$PMB_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Select the device using 'pmbootstrap init'.\033[0m"
		exit 1
	fi

	CommonPackages="`ls $OWN_APORTS_PATH | grep $OWN_DEVICE_CHIP || true`"
	DevicePackages="`ls $OWN_APORTS_PATH | grep $OWN_DEVICE_NAME || true`"
	
	if [ ! -z "$2" ]
	then
		CommonPackages="`echo "$CommonPackages" | grep $2 || true`"
		DevicePackages="`echo "$DevicePackages" | grep $2 || true`"
	fi

	Packages="$CommonPackages $DevicePackages"

	TotalSteps=`expr $(echo $Packages | wc -w) + 1`

	if [ $TotalSteps -eq 1 ]
	then
		echo -e "\033[1m[!] Zero packages selected.\033[0m"
		exit 1
	fi

	echo -e "\033[1m[1/$TotalSteps] Generating checksums for device packages\033[0m"
	for Package in $Packages
	do
		pmbootstrap checksum $Package
	done

	CurrentStep=2
	for Package in $Packages
	do
		echo -e "\033[1m[$CurrentStep/$TotalSteps] Building package - $Package\033[0m"
		pmbootstrap build --force $Package
		CurrentStep=`expr $CurrentStep + 1`
	done

	exit 0
fi



# Command - Install the packages and prepare flashables
# Usage: auto.sh install

if [ "$1" = "install" ]
then
	if [ -z "$OWN_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Initialize with 'init' first.\033[0m"
		exit 1
	fi

	if [ ! "$OWN_DEVICE_NAME" = "$PMB_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Select the device using 'pmbootstrap init'.\033[0m"
		exit 1
	fi

	echo -e "\033[1m[1/1] Installing postmarketOS and creating flashable images\033[0m"
	pmbootstrap install --password 147147 --sparse

	exit 0
fi



# Command - Flash the images
# Usage: auto.sh flash [kernel]

if [ "$1" = "flash" ]
then
	if [ -z "$OWN_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Initialize with 'init' first.\033[0m"
		exit 1
	fi

	if [ ! "$OWN_DEVICE_NAME" = "$PMB_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Select the device using 'pmbootstrap init'.\033[0m"
		exit 1
	fi

	if [ "$2" = "kernel" ]
	then
		echo -e "\033[1m[1/1] Flashing the boot image\033[0m"
		pmbootstrap flasher flash_kernel

	elif [ -z "$2" ]
	then
		if [ -e "$PMB_CHROOT_PATH/home/pmos/rootfs/$OWN_DEVICE_NAME.img" ]
		then
			echo -e "\033[1m+
| Since download mode allows a single flash at a time, a
| reboot is required to flash the rootfs and boot images
| respectively.
|
| Please be ready for pressing the key combination for your
| device to reboot to download mode as soon as the first image
| is flashed.
+
			\033[0m"

			echo -e "\033[1m[1/2] Flashing the boot image\033[0m"
			pmbootstrap flasher flash_kernel

			echo -e "\033[1m[2/2] Flashing the rootfs image\033[0m"
			pmbootstrap flasher flash_rootfs

		else
			echo -e "\033[1m[!] Install the packages with 'install' first.\033[0m"
			exit 1
		fi

	else
		echo -e "\033[1m[!] Invalid argument '$2'.\033[0m"
		exit 1
	fi

	exit 0
fi



# Command - Export the recovery-flashable zip
# Usage: auto.sh export

if [ "$1" = "export" ]
then
	if [ -z "$OWN_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Initialize with 'init' first.\033[0m"
		exit 1
	fi

	if [ ! "$OWN_DEVICE_NAME" = "$PMB_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Select the device using 'pmbootstrap init'.\033[0m"
		exit 1
	fi

	if [ -e "$PMB_CHROOT_PATH/home/pmos/rootfs/$OWN_DEVICE_NAME.img" ]
	then
		echo -e "\033[1m[1/1] Exporting flashable images\033[0m"
		pmbootstrap export --no-install
		cp "/tmp/postmarketOS-export/boot.img" .
		cp "/tmp/postmarketOS-export/$OWN_DEVICE_NAME.img" .

	else
		echo -e "\033[1m[!] Install the packages with 'install' first.\033[0m"
		exit 1
	fi

	exit 0
fi



# Command - Run CI test in pmbootstrap
# Usage: auto.sh ci

if [ "$1" = "test" ]
then
	if [ -z "$OWN_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Initialize with 'init' first.\033[0m"
		exit 1
	fi

	if [ ! "$OWN_DEVICE_NAME" = "$PMB_DEVICE_NAME" ]
	then
		echo -e "\033[1m[!] Select the device using 'pmbootstrap init'.\033[0m"
		exit 1
	fi

	Packages="`ls $OWN_APORTS_PATH | grep "$OWN_DEVICE_CHIP\|$OWN_DEVICE_NAME" || true`"

	echo -e "\033[1m[1/2] Generating checksums for device packages\033[0m"
	for Package in $Packages
	do
		pmbootstrap checksum $Package
	done
	
	cd $PMB_APORTS_PATH
	HeadCommitHash=`git rev-parse HEAD`

	for Package in $Packages
	do
		git add $Package
	done

	git -c "user.name=ci" -c "user.email=ci@example.org" commit -m "CI" > /dev/null 2>&1
	
	echo -e "\033[1m[2/2] Beginning CI test\033[0m"
	
	trap - INT
	pmbootstrap ci || true
	trap "exit 1" INT
	
	git reset $HeadCommitHash > /dev/null 2>&1

	exit 0
fi



# Discard invalid commands

if [ ! -z "$1" ]
then
	echo -e "\033[1m[!] Invalid argument '$1'.\033[0m"
	exit 1
fi



## auto.sh ##

echo -e "\033[1m
 _     _                  _____          _____
| |   (_)_ __  _   ___  _|  ___|__  _ __| ____|_  ___   _ _ __   ___  ___
| |   | | '_ \| | | \ \/ / |_ / _ \| '__|  _| \ \/ / | | | '_ \ / _ \/ __|
| |___| | | | | |_| |>  <|  _| (_) | |  | |___ >  <| |_| | | | | (_) \__ \\
|_____|_|_| |_|\__,_/_/\_\_|  \___/|_|  |_____/_/\_\__,  |_| |_|\___/|___/
                                                    |___/                 
                 _                        _        _    ___  ____
 _ __   ___  ___| |_ _ __ ___   __ _ _ __| | _____| |_ / _ \/ ___|
| '_ \ / _ \/ __| __| '_  _  \ / _  | '__| |/ / _ \ __| | | \___ \\
| |_) | (_) \__ \ |_| | | | | | (_| | |  |   <  __/ |_| |_| |___) |
| .__/ \___/|___/\__|_| |_| |_|\__,_|_|  |_|\_\___|\__|\___/|____/
|_|
    _         _                          _
   / \  _   _| |_ _____      _____  _ __| | _____ _ __
  / _ \| | | | __/ _ \ \ /\ / / _ \| '__| |/ / _ \ '__|
 / ___ \ |_| | || (_) \ V  V / (_) | |  |   <  __/ |
/_/   \_\__,_|\__\___/ \_/\_/ \___/|_|  |_|\_\___|_|
\033[0m"

echo -e "\033[1m=> auto.sh build\033[1m"
$0 build

echo

echo -e "\033[1m=> auto.sh install\033[1m"
$0 install
